#Inicio de método de finabocci
def fibonacci(numero):
    if numero < 2:
        return numero
    else:
        return fibonacci(numero - 1) + fibonacci(numero - 2)

#BugFix 201325641
def solicitarNumero():
    print("Ingrese un número: ")
    numero = input()
    print(f"Fibonacci del número {(numero)} es: {fibonacci(int(numero))}")

solicitarNumero()
    
#Inicio de método de números primos
def esNumeroPrimo(numero):
    if numero < 1:
        return False
    elif numero == 2:
        return True
    else:
        for i in range(2, numero):
            if numero % i == 0:
                return False
        return True            


numero = int(input('Ingresa un número: '))
resultado = esNumeroPrimo(numero)
if resultado:
    print('El número ' + str(numero) + ' es primo.')
else:
    print('El número ' + str(numero) + ' no es primo.')

print("mi bugfix-201602999")